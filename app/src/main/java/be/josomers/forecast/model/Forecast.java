package be.josomers.forecast.model;

/**
 * @author Jo Somers
 */
public class Forecast {

    private final String prediction;

    public Forecast(String prediction) {
        this.prediction = prediction;
    }

    public String getPrediction() {
        return prediction;
    }
}
