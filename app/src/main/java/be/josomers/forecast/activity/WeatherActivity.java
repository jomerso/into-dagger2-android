package be.josomers.forecast.activity;

import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

import be.josomers.forecast.R;
import be.josomers.forecast.core.BaseActivity;
import be.josomers.forecast.model.Forecast;
import be.josomers.forecast.service.Callback;
import be.josomers.forecast.service.ForecastService;
import butterknife.Bind;
import dagger.Lazy;

/**
 * @author Jo Somers
 */
public class WeatherActivity extends BaseActivity {

    @Inject
    Lazy<ForecastService> lazyWeatherService;

    @Bind(R.id.forecast_predication)
    TextView forecastPredicationTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        refreshWeatherForecastForLondonUK();
    }

    @Override
    public int getContentViewResourceId() {
        return R.layout.activity_main;
    }

    private void refreshWeatherForecastForLondonUK() {
        lazyWeatherService.get().getWeatherForecastInLondonUK(new Callback<Forecast>() {
            @Override
            public void handle(Forecast forecast) {
                forecastPredicationTextView.setText(forecast.getPrediction());
            }
        });
    }
}
