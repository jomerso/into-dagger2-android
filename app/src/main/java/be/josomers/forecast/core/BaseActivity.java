package be.josomers.forecast.core;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import be.josomers.forecast.core.BaseApplication.ApplicationComponent;
import butterknife.ButterKnife;

/**
 * @author Jo Somers
 */
public abstract class BaseActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        setContentView(getContentViewResourceId());
        ButterKnife.bind(this);
    }

    public ApplicationComponent getComponent() {
        return getBaseApplication().getComponent();
    }

    public BaseApplication getBaseApplication() {
        return (BaseApplication) getApplication();
    }

    public abstract int getContentViewResourceId();

}
