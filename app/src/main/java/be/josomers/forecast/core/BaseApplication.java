package be.josomers.forecast.core;

import android.app.Application;

import javax.inject.Singleton;

import be.josomers.forecast.activity.WeatherActivity;
import be.josomers.forecast.module.WeatherServiceModule;
import dagger.Component;

/**
 * @author Jo Somers
 */
public class BaseApplication extends Application {

    @Singleton
    @Component(modules = WeatherServiceModule.class)
    public interface ApplicationComponent {
        void inject(BaseApplication application);
        void inject(BaseActivity baseActivity);
        void inject(WeatherActivity homeActivity);
    }

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        if (component == null) {
            component = DaggerBaseApplication_ApplicationComponent.builder().build();
        }

        getComponent().inject(this);
    }

    public ApplicationComponent getComponent() {
        return component;
    }

    public void setComponent(ApplicationComponent component) {
        this.component = component;
    }
}
