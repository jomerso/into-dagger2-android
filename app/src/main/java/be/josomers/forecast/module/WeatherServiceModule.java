package be.josomers.forecast.module;

import be.josomers.forecast.service.ForecastService;
import be.josomers.forecast.service.RestForecastService;
import dagger.Module;
import dagger.Provides;

/**
 * @author Jo Somers
 */
@Module
public class WeatherServiceModule {

    @Provides
    ForecastService provides() {
        return new RestForecastService();
    }
}
