package be.josomers.forecast.service;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import be.josomers.forecast.model.Forecast;
import be.josomers.forecast.service.model.ForecastOutput;

/**
 * @author Jo Somers
 */
public class RestForecastService implements ForecastService {

    @Override
    public void getWeatherForecastInLondonUK(final Callback<Forecast> callback) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                final String fullUrl = "http://api.openweathermap.org/data/2.5/weather?q=London,uk";

                HttpURLConnection connection = null;
                try {
                    final URL url = new URL(fullUrl);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(10000);
                    connection.setReadTimeout(30000);

                    final InputStream stream = connection.getInputStream();
                    final String response = IOUtils.toString(stream);
                    final ForecastOutput forecastOutput = new Gson().fromJson(response,
                            new TypeToken<ForecastOutput>() {
                            }.getType());

                    return forecastOutput.getWeatherOutput().toString();
                } catch (Exception e) {
                    return "No weather available.";
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }

            @Override
            protected void onPostExecute(String predication) {
                super.onPostExecute(predication);
                callback.handle(new Forecast(predication));
            }
        }.execute();
    }
}
