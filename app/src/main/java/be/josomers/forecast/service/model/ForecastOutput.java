package be.josomers.forecast.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Jo Somers
 */
public class ForecastOutput {

    @SerializedName("weather")
    private List<WeatherOutput> weatherOutput;

    public List<WeatherOutput> getWeatherOutput() {
        return weatherOutput;
    }

    public void setWeatherOutput(List<WeatherOutput> weatherOutput) {
        this.weatherOutput = weatherOutput;
    }
}
