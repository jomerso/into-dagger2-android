package be.josomers.forecast.service;

import be.josomers.forecast.model.Forecast;

/**
 * @author Jo Somers
 */
public interface ForecastService {

    void getWeatherForecastInLondonUK(Callback<Forecast> callback);

}
