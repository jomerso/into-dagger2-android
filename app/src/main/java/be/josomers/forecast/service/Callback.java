package be.josomers.forecast.service;

/**
 * @author Jo Somers
 */
public interface Callback<T> {
    void handle(T output);
}
