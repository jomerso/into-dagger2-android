package be.josomers.forecast.service;

import be.josomers.forecast.model.Forecast;

/**
 * @author Jo Somers
 */
public class MockForecastService implements ForecastService {

    @Override
    public void getWeatherForecastInLondonUK(Callback<Forecast> callback) {
        callback.handle(new Forecast("[MOCK] Always look on the bright side of life [/MOCK]"));
    }
}
