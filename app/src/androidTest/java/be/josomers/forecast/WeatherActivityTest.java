package be.josomers.forecast;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Singleton;

import be.josomers.forecast.activity.WeatherActivity;
import be.josomers.forecast.core.BaseApplication;
import be.josomers.forecast.core.BaseApplication.ApplicationComponent;
import be.josomers.forecast.module.MockWeatherServiceModule;
import dagger.Component;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * @author Jo Somers
 */
@RunWith(AndroidJUnit4.class)
public class WeatherActivityTest {

    @Singleton
    @Component(modules = MockWeatherServiceModule.class)
    public interface TestApplicationComponent extends ApplicationComponent {
    }

    @Rule
    public ActivityTestRule<WeatherActivity> mActivityRule
            = new ActivityTestRule(WeatherActivity.class, true, false);

    @Before
    public void setUp() {
        getBaseApplication().setComponent(DaggerWeatherActivityTest_TestApplicationComponent.builder().build());
    }

    @Test
    public void textOnViewAfterRequest() {
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.forecast_predication))
                .check(matches(withText("[MOCK] Always look on the bright side of life [/MOCK]")));
    }

    private BaseApplication getBaseApplication() {
        return (BaseApplication) InstrumentationRegistry.getInstrumentation().getTargetContext().getApplicationContext();
    }

}
