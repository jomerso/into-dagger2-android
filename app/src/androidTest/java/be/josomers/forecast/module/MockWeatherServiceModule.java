package be.josomers.forecast.module;

import be.josomers.forecast.service.ForecastService;
import be.josomers.forecast.service.MockForecastService;
import dagger.Module;
import dagger.Provides;

/**
 * @author Jo Somers
 */
@Module
public class MockWeatherServiceModule {

    @Provides
    ForecastService provides() {
        return new MockForecastService();
    }
}
